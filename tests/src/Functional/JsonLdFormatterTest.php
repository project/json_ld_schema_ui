<?php

namespace Drupal\Tests\json_ld_schema_ui\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the JSON-LD output of entities.
 *
 * @group json_ld_schema_ui
 */
class JsonLdFormatterTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['json_ld_schema_ui', 'node'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The schema content settings storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $schemaContentSettingsStorage;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $node_type_storage->save($node_type_storage->create(['type' => 'article']));

    $this->schemaContentSettingsStorage = $entity_type_manager->getStorage('schema_content_settings');
    $this->nodeStorage = $entity_type_manager->getStorage('node');

    $this->drupalLogin($this->drupalCreateUser(['access content']));
  }

  /**
   * Tests the JSON-LD output of entities.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testOutput() {
    // Do not use a data provider for this to avoid re-installing Drupal for
    // each test case.
    $tests = [];

    // Test basic JSON-LD output with configured defaults.
    $test = [
      'properties_configuration' => [
        'name' => [
          'default_value' => 'Test article',
          'allow_override' => FALSE,
        ],
        'description' => [
          'default_value' => 'This is a test article',
          'allow_override' => FALSE,
        ],
      ],
      'node_values' => [
        'type' => 'article',
        'title' => 'Test article',
      ],
      'output' => '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Test article","description":"This is a test article"}</script>',
    ];
    $tests['default'] = $test;

    // Test using tokens in the defaults.
    $test['properties_configuration']['name']['default_value'] = '[node:title]';
    $test['properties_configuration']['description']['default_value'] = 'This is a [node:title]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Test article","description":"This is a Test article"}</script>';
    $tests['default-token'] = $test;

    // Test that a missing token is not cleared.
    $test['properties_configuration']['description']['default_value'] = 'This description does not have a [token]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Test article","description":"This description does not have a [token]"}</script>';
    $tests['default-token-missing'] = $test;

    // Test that overriding defaults works.
    $test['properties_configuration']['name']['allow_override'] = TRUE;
    $test['properties_configuration']['description']['allow_override'] = TRUE;
    $test['node_values']['jsonld_schema']['overrides']['Article']['name'] = 'Overridden schema name';
    $test['node_values']['jsonld_schema']['overrides']['Article']['description'] = 'Overridden schema description';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden schema name","description":"Overridden schema description"}</script>';
    $tests['override'] = $test;

    // Test that overriding with tokens works.
    $test['node_values']['jsonld_schema']['overrides']['Article']['name'] = 'Overridden [node:title]';
    $test['node_values']['jsonld_schema']['overrides']['Article']['description'] = 'This is an overridden [node:title]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","description":"This is an overridden Test article"}</script>';
    $tests['override-token'] = $test;

    // Test that a missing token is not cleared from an override.
    $test['node_values']['jsonld_schema']['overrides']['Article']['description'] = 'This overridden description does not have a [token]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","description":"This overridden description does not have a [token]"}</script>';
    $tests['override-token-missing'] = $test;

    // Test support for nested properties.
    unset(
      $test['properties_configuration']['description'],
      $test['node_values']['jsonld_schema']['overrides']['Article']['description']
    );
    $test['properties_configuration']['comment'] = [
      'properties' => [
        '@type' => [
          'default_value' => 'Comment',
          'allow_override' => FALSE,
        ],
        'text' => [
          'default_value' => 'Test comment about the test article',
          'allow_override' => FALSE,
        ],
      ],
    ];
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","comment":{"@type":"Comment","text":"Test comment about the test article"}}</script>';
    $tests['nested'] = $test;

    // Test token support with nested properties.
    $test['properties_configuration']['comment']['properties']['text']['default_value'] = 'Test comment about [node:title]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","comment":{"@type":"Comment","text":"Test comment about Test article"}}</script>';
    $tests['nested-token'] = $test;

    // Test that a missing token is not cleared in a nested property.
    $test['properties_configuration']['comment']['properties']['text']['default_value'] = 'This comment does not have a [token]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","comment":{"@type":"Comment","text":"This comment does not have a [token]"}}</script>';
    $tests['nested-token-missing'] = $test;

    // Test that overriding nested properties works.
    $test['properties_configuration']['comment']['properties']['text']['allow_override'] = TRUE;
    $test['node_values']['jsonld_schema']['overrides']['Article']['comment|text'] = 'Overridden schema comment';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","comment":{"@type":"Comment","text":"Overridden schema comment"}}</script>';
    $tests['nested-override'] = $test;

    // Test that overriding nested properties with tokens work.
    $test['node_values']['jsonld_schema']['overrides']['Article']['comment|text'] = 'Overridden comment about [node:title]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","comment":{"@type":"Comment","text":"Overridden comment about Test article"}}</script>';
    $tests['nested-override-token'] = $test;

    // Test that a missing token is not cleared in an overridden, nested
    // property.
    $test['node_values']['jsonld_schema']['overrides']['Article']['comment|text'] = 'This overridden comment does not have a [token]';
    $test['output'] = '<script type="application/ld+json">{"@context":"http:\/\/schema.org\/","@type":"Article","name":"Overridden Test article","comment":{"@type":"Comment","text":"This overridden comment does not have a [token]"}}</script>';
    $tests['nested-override-token-missing'] = $test;

    /** @var \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings $content_schema_settings */
    $content_schema_settings = $this->schemaContentSettingsStorage->create([
      'target_entity_type' => 'node',
      'target_bundle' => 'article',
      'schema_type' => 'Article',
    ]);

    foreach ($tests as $test) {
      $content_schema_settings
        ->set('schema_properties', $test['properties_configuration'])
        ->save();

      $node = $this->nodeStorage->create($test['node_values']);
      $node->save();

      $this->drupalGet($node->toUrl());
      $this->assertSession()->responseContains($test['output']);
    }
  }

}
