<?php

/**
 * @file
 * Contains hook implementations for the JSON-LD Schema UI module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Drupal\entity\BundleFieldDefinition;

/**
 * Implements hook_entity_field_storage_info().
 */
function json_ld_schema_ui_entity_field_storage_info(EntityTypeInterface $entity_type) {
  $fields = [];

  $entity_type_id = $entity_type->id();
  if (\Drupal::configFactory()->listAll("json_ld_schema_ui.content_settings.$entity_type_id.")) {
    $fields['jsonld_schema'] = BundleFieldDefinition::create('jsonld')
      // @todo Setting these two explicitly should not be necessary.
      ->setName('jsonld_schema')
      ->setTargetEntityTypeId($entity_type_id)
      ->setLabel(t('JSON-LD Schemas'))
      ->setCardinality(1);
  }
  return $fields;
}

/**
 * Implements hook_entity_bundle_field_info().
 */
function json_ld_schema_ui_entity_bundle_field_info(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
  $fields = [];

  $entity_type_id = $entity_type->id();
  $config_names = \Drupal::configFactory()->listAll("json_ld_schema_ui.content_settings.$entity_type_id.$bundle.");
  if ($config_names) {
    $fields['jsonld_schema'] = BundleFieldDefinition::create('jsonld')
      ->setLabel(t('JSON-LD Schemas'))
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', ['weight' => 40])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'weight' => 100,
      ]);
  }
  return $fields;
}

/**
 * Implements hook_entity_bundle_info_alter().
 *
 * @todo Check if this is even necessary or functional. Think it should be moved
 *       to the ContentSchemaSettings entity's label() method.
 *
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function json_ld_schema_ui_entity_bundle_info_alter(&$all_bundles) {
  $all_bundles['schema_content_settings'] = [];
  foreach ($all_bundles as $entity_type_id => $bundles) {
    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    if (($entity_type->getGroup() === 'content') && $entity_type->hasViewBuilderClass()) {
      foreach ($bundles as $bundle => $bundle_info) {
        if (((string) $entity_type->getLabel()) === ((string) $bundle_info['label'])) {
          $label = $bundle_info['label'];
        }
        else {
          $label = t('@entity-type: @bundle', [
            '@entity-type' => $entity_type->getLabel(),
            '@bundle' => $bundle_info['label'],
          ]);
        }
        $all_bundles['schema_content_settings'][$entity_type_id . '.' . $bundle] = [
          'label' => $label,
        ];
      }
    }
  }

  // Sort the list of bundles so it will show up correctly in the user
  // interface.
  // @todo Fix this upstream in
  //   https://www.drupal.org/project/drupal/issues/2693485 and remove
  //   this workaround
  $labels = array_map('strval', array_column($all_bundles['schema_content_settings'], 'label'));
  array_multisort($labels, SORT_NATURAL & SORT_FLAG_CASE, $all_bundles['schema_content_settings']);
}

/**
 * Implements hook_entity_operation().
 */
function json_ld_schema_ui_entity_operation(EntityInterface $entity) {
  $operations = [];

  if (($bundle_of = $entity->getEntityType()->getBundleOf()) && \Drupal::entityTypeManager()->getDefinition($bundle_of)->get('field_ui_base_route')) {
    $account = \Drupal::currentUser();
    if ($account->hasPermission('administer content schema settings')) {
      $operations['manage-json-ld-schema'] = [
        'title' => t('Manage JSON LD schema'),
        'weight' => 90,
        'url' => Url::fromRoute("entity.{$bundle_of}.json_ld_schema_ui", [
          $entity->getEntityTypeId() => $entity->id(),
        ]),
      ];
    }
  }

  return $operations;
}
