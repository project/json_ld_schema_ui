<?php

namespace Drupal\json_ld_schema_ui\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for JSON LD schema UI routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // We can piggyback on to op field UI to find the bundle routes.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($route_name = $entity_type->get('field_ui_base_route')) {
        // Try to get the route from the current collection.
        if (!$entity_route = $collection->get($route_name)) {
          continue;
        }
        $path = $entity_route->getPath();
        $options = $entity_route->getOptions();

        if ($bundle_entity_type = $entity_type->getBundleEntityType()) {
          $options['parameters'][$bundle_entity_type] = [
            'type' => 'entity:' . $bundle_entity_type,
          ];
        }
        else {
          continue;
        }

        $defaults = ['bundle_entity_type' => $bundle_entity_type];

        $route = new Route(
          "$path/json_ld_schema",
          [
            '_form' => '\Drupal\json_ld_schema_ui\Form\EntitySchemaConfigurationForm',
            '_title' => 'Manage JSON LD schema',
          ] + $defaults,
          ['_permission' => 'administer content schema settings'],
          $options
        );
        $collection->add("entity.{$entity_type_id}.json_ld_schema_ui", $route);

        $route = new Route(
          "$path/json_ld_schema/add_property/{schema_type}/{property_path}",
          [
            '_form' => '\Drupal\json_ld_schema_ui\Form\EntitySchemaAddPropertyForm',
            '_title_callback' => '\Drupal\json_ld_schema_ui\Form\EntitySchemaAddPropertyForm::formTitle',
          ] + $defaults,
          ['_permission' => 'administer content schema settings'],
          $options
        );
        $collection->add("entity.{$entity_type_id}.json_ld_schema_ui.add_property", $route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -100];
    return $events;
  }

}
