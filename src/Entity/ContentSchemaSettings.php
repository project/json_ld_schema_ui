<?php

namespace Drupal\json_ld_schema_ui\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the ContentSchemaSettings entity.
 *
 * @see \Drupal\language\Entity\ContentLanguageSettings
 *
 * @ConfigEntityType(
 *   id = "schema_content_settings",
 *   label = @Translation("Content Schema Settings"),
 *   label_collection = @Translation("Content Schema Settings"),
 *   label_singular = @Translation("content schema setting"),
 *   label_plural = @Translation("content schema settings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count content schema setting",
 *     plural = "@count content schema settings",
 *   ),
 *   config_export = {
 *     "id",
 *     "bundle",
 *     "target_entity_type",
 *     "target_bundle",
 *     "schema_type",
 *     "schema_properties",
 *   },
 *   admin_permission = "administer content schema settings",
 *   config_prefix = "content_settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *   },
 *   list_cache_tags = { "entity_field_info" },
 * )
 */
class ContentSchemaSettings extends ConfigEntityBase {

  use StringTranslationTrait;

  /**
   * The ID.
   *
   * This is comprised of target entity type ID, bundle name and schema class.
   *
   * @var string
   */
  protected $id;

  /**
   * The bundle.
   *
   * This is comprised of target entity type ID and bundle name.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $target_entity_type;

  /**
   * The bundle.
   *
   * @var string
   */
  protected $target_bundle;

  /**
   * The schema type.
   *
   * @var string
   */
  protected $schema_type;

  /**
   * An array of schema property configuration.
   *
   * Each value is an array with either of the following keys:
   * - default_value: The property's default value.
   * - properties: A mapping of nested property configuration for reference
   *   properties.
   *
   * @var array[]
   */
  protected $schema_properties = [];

  /**
   * Constructs a ContentSchemaSettings object.
   *
   * @param array $values
   *   An array of the referring entity bundle with:
   *   - target_entity_type_id: The entity type.
   *   - target_bundle: The bundle.
   *   Other array elements will be used to set the corresponding properties on
   *   the class; see the class property documentation for details.
   * @param string $entity_type
   *   The entity type.
   *
   * @see \Drupal\Core\Entity\EntityStorageInterface::create()
   */
  public function __construct(array $values, string $entity_type = 'schema_content_settings') {
    if (empty($values['target_entity_type'])) {
      throw new \RuntimeException('Attempt to create content schema settings without a target entity type.');
    }
    if (empty($values['target_bundle'])) {
      throw new \RuntimeException('Attempt to create content schema settings without a target bundle.');
    }
    if (empty($values['schema_type'])) {
      throw new \RuntimeException('Attempt to create content schema settings without a schema type.');
    }
    parent::__construct($values, $entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->target_entity_type . '.' . $this->target_bundle . '.' . strtolower($this->schema_type);
  }

  /**
   * {@inheritdoc}
   */
  public function bundle() {
    return $this->target_entity_type . '.' . $this->target_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle() {
    return $this->target_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaType() {
    return $this->schema_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaProperties() {
    return $this->schema_properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getOverridableProperties() {
    $recursive_check = function (array $properties, array &$filtered) use (&$recursive_check) {
      foreach ($properties as $label => $property) {
        if ($label == '@type') {
          continue;
        }

        // Recurse over nested properties.
        if (!empty($property['properties'])) {
          $sub_properties = [];
          $recursive_check($property['properties'], $sub_properties);
          if (empty($sub_properties)) {
            continue;
          }
          $property['properties'] = $sub_properties;
          $filtered[$label] = $property;
        }

        // If the element has no nested properties, check if allowed override.
        if (!empty($property['allow_override'])) {
          $filtered[$label] = $property;
        }
      }
    };

    $overridable_properties = [];
    $recursive_check($this->schema_properties, $overridable_properties);

    return $overridable_properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    // @todo Re-evaluate the bundle property, I really don't think it's needed.
    if (!empty($values['bundle'])) {
      [$target_entity_type, $target_bundle] = explode('.', $values['bundle']);
      $values += [
        'target_entity_type' => $target_entity_type,
        'target_bundle' => $target_bundle,
      ];
    }
    elseif (!empty($values['target_entity_type']) && !empty($values['target_bundle'])) {
      $values['bundle'] = $values['target_entity_type'] . '.' . $values['target_bundle'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if (empty($this->target_entity_type)) {
      throw new \RuntimeException('Attempt to save content schema settings without a target entity type.');
    }
    if (empty($this->target_bundle)) {
      throw new \RuntimeException('Attempt to save content schema settings without a target bundle.');
    }
    if (empty($this->schema_type)) {
      throw new \RuntimeException('Attempt to save content schema settings without a schema type.');
    }
    $this->id = $this->id();
    $this->bundle = $this->bundle();

    parent::preSave($storage);

    $config_factory = \Drupal::configFactory();
    // If this is the first schema configuration for this entity type, install
    // the JSON-LD field.
    if (!$config_factory->listAll("json_ld_schema_ui.content_settings.{$this->target_entity_type}.")) {
      $update_manager = \Drupal::entityDefinitionUpdateManager();

      $storage_definition = BundleFieldDefinition::create('jsonld')
        ->setName('jsonld_schema')
        ->setLabel($this->t('JSON-LD Schemas'))
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
      $update_manager->installFieldStorageDefinition('jsonld_schema', $this->target_entity_type, 'json_ld_schema_ui', $storage_definition);

      // @todo Update form and view displays.
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    /** @var \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings $entity */
    $target_entity_type_ids = [];
    foreach ($entities as $entity) {
      $target_entity_type_ids[] = $entity->getTargetEntityTypeId();
    }
    $target_entity_type_ids = array_unique($target_entity_type_ids);

    $config_factory = \Drupal::configFactory();
    $update_manager = \Drupal::entityDefinitionUpdateManager();
    foreach ($target_entity_type_ids as $target_entity_type_id) {
      // If the last schema configuration for this entity type was deleted,
      // uninstallthe JSON-LD field.
      if (!$config_factory->listAll("json_ld_schema_ui.content_settings.$target_entity_type_id")) {
        /** @var \Drupal\entity\BundleFieldDefinition $storage_definition */
        $storage_definition = BundleFieldDefinition::create('jsonld')
          ->setName('jsonld_schema')
          ->setTargetEntityTypeId($target_entity_type_id)
          ->setProvider('json_ld_schema_ui')
          ->setLabel(t('JSON-LD Schemas'))
          ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
        $update_manager->uninstallFieldStorageDefinition($storage_definition);

        // @todo Update form and view displays.
      }
    }

    parent::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    // Create dependency on the bundle.
    $entity_type = \Drupal::entityTypeManager()->getDefinition($this->target_entity_type);
    $bundle_config_dependency = $entity_type->getBundleConfigDependency($this->target_bundle);
    $this->addDependency($bundle_config_dependency['type'], $bundle_config_dependency['name']);

    return $this;
  }

}
