<?php

namespace Drupal\json_ld_schema_ui\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Provides a formatter to add JSON-LD output to the website <head>.
 *
 * @FieldFormatter(
 *   id = "jsonld_head",
 *   label = @Translation("JSON-LD in website &lt;head&gt;"),
 *   field_types = {
 *     "jsonld",
 *   },
 * )
 */
class JsonLdHeadFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = ['#attached' => ['html_head' => []]];
    $entity = $items->getEntity();

    // Skip adding jsonld schema for non saved yet entity.
    if ($entity->isNew()) {
      return [];
    }

    // When there is no item, default to the configured values without any
    // overrides. See JsonLdItem::isEmpty().
    if (count($items) === 0) {
      $entity->set('jsonld_schema', ['overrides' => []]);
      $items = $entity->get('jsonld_schema');
    }

    foreach ($items as $delta => $item) {
      /** @var \Drupal\json_ld_schema_ui\TypedData\JsonLdProcessed $processed */
      $processed = $item->get('processed');
      $elements['#attached']['html_head'][] = [
        [
          '#tag' => 'script',
          '#attributes' => ['type' => 'application/ld+json'],
          '#value' => $processed->getValue(),
        ],
        sprintf(
          'json_ld_%s_%s_%s_%d',
          $entity->getEntityTypeId(),
          $entity->id(),
          $items->getFieldDefinition()->getName(),
          $delta
        ),
      ];

      CacheableMetadata::createFromObject($processed)->applyTo($elements);
    }

    return $elements;
  }

}
