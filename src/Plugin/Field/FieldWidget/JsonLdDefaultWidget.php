<?php

namespace Drupal\json_ld_schema_ui\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a widget to configure JSON-LD schema configuration.
 *
 * @FieldWidget(
 *   id = "jsonld_default",
 *   label = @Translation("JSON-LD Schema Configuration"),
 *   field_types = {
 *     "jsonld",
 *   },
 * )
 */
class JsonLdDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The schema data.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface
   */
  protected $schemaData;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->schemaData = $container->get('json_ld_schema_ui.schema');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $items->getFieldDefinition()->getName();
    $widget_parents = array_merge(
      $element['#field_parents'],
      [$field_name, $delta]
    );
    $schema_configs = $this->getSchemaConfigs($items);

    // Show something about configuring it?
    if (count($schema_configs) == 0) {
      return $element;
    }

    $overrides = $items[$delta]->get('overrides')->getValue();
    foreach ($schema_configs as $schema_config) {
      $overridable_properties = $schema_config->getOverridableProperties();
      if (empty($overridable_properties)) {
        continue;
      }

      $schema_type = $schema_config->getSchemaType();
      $element[$schema_type] = [
        '#type' => 'details',
        '#title' => $this->t('Schema.org data for @type', ['@type' => $schema_type]),
      ];

      $element[$schema_type]['table'] = [
        '#type' => 'table',
        '#header' => [
          'label' => $this->t('Label'),
          'value' => $this->t('Value'),
          'override' => $this->t('Override'),
        ],
      ];

      $table_parents = array_merge($widget_parents, [$schema_type, 'table']);
      $schema_overrides = $overrides[$schema_type] ?? [];
      $row_builder = function ($properties, $property_path = []) use (&$row_builder, $table_parents, $schema_overrides) {
        $rows = [];

        foreach ($properties as $label => $property) {
          $row_property_path = array_merge($property_path, [$label]);
          $row_property_key = implode('|', $row_property_path);
          $row_property_label = implode(' > ', $row_property_path);
          $row = &$rows[$row_property_key];

          if (empty($property['properties'])) {
            $row_parents = array_merge($table_parents, [$row_property_key]);
            if (isset($schema_overrides[$row_property_key])) {
              $defaults = [
                'value' => $schema_overrides[$row_property_key],
                'override' => TRUE,
              ];
            }
            else {
              $defaults = [
                'value' => $property['default_value'] ?: '',
                'override' => FALSE,
              ];
            }

            $row['label'] = ['#markup' => $row_property_label];

            $checkbox_parents = array_merge($row_parents, ['override']);
            $checkbox_path = array_shift($checkbox_parents);
            $checkbox_path .= '[' . implode('][', $checkbox_parents) . ']';
            $row['value'] = [
              '#type' => 'textfield',
              '#title' => $this->t('Value'),
              '#title_display' => 'invisible',
              '#default_value' => $defaults['value'],
              '#states' => [
                'enabled' => [
                  ':input[name="' . $checkbox_path . '"]' => ['checked' => TRUE],
                ],
              ],
            ];

            if (!empty($property['enum_type'])) {
              $row['value']['#type'] = 'select';
              $row['value']['#options'] = $this->schemaData->getEnumOptions($property['enum_type']);
            }

            $row['override'] = [
              '#type' => 'checkbox',
              '#title' => $this->t('Override'),
              '#title_display' => 'invisible',
              '#default_value' => $defaults['override'],
            ];
          }
          else {
            $rows += $row_builder($property['properties'], $row_property_path);
          }
        }

        return $rows;
      };

      $rows = $row_builder($overridable_properties);
      asort($rows);
      $element[$schema_type]['table'] += $rows;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $element = parent::form($items, $form, $form_state, $get_delta);
    $schema_configs = $this->getSchemaConfigs($items);

    // Show something about configuring it?
    if (count($schema_configs) == 0) {
      return $element;
    }

    $overrides_possible = FALSE;
    foreach ($schema_configs as $schema_config) {
      $overridable_properties = $schema_config->getOverridableProperties();
      if (empty($overridable_properties)) {
        continue;
      }
      $overrides_possible = TRUE;
      break;
    }

    // Show something about configuring it?
    if (!$overrides_possible) {
      return $element;
    }

    $element = [
      '#type' => 'details',
      '#title' => $this->t('JSON-LD schema settings'),
      '#access' => $items->access('edit'),
      // Show this right above moderation state.
      '#weight' => 99,
    ] + $element;

    if ($this->moduleHandler->moduleExists('token')) {
      $element['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [$items->getEntity()->getEntityTypeId()],
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$item_values) {
      $overrides = [];

      foreach ($item_values as $schema_type => $schema_data) {
        if (!is_array($schema_data) || empty($schema_data['table'])) {
          continue;
        }

        foreach ($schema_data['table'] as $property_path => $property_values) {
          if (empty($property_values['override'])) {
            continue;
          }
          $overrides[$schema_type][$property_path] = $property_values['value'];
        }
      }

      $item_values['overrides'] = $overrides;
    }

    return $values;
  }

  /**
   * Loads the schema config for the field items.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field items.
   *
   * @return \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings[]
   *   The schema configs, if any.
   */
  protected function getSchemaConfigs(FieldItemListInterface $items) {
    $entity = $items->getEntity();
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    return $this->entityTypeManager
      ->getStorage('schema_content_settings')
      ->loadByProperties(['bundle' => "$entity_type_id.$bundle"]);
  }

}
