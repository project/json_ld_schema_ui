<?php

namespace Drupal\json_ld_schema_ui\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\json_ld_schema_ui\TypedData\JsonLdProcessed;
use Drupal\json_ld_schema_ui\TypedData\SchemaBundle;

/**
 * Defines the 'jsonld' field type.
 *
 * @FieldType(
 *   id = "jsonld",
 *   label = @Translation("JSON-LD"),
 *   description = @Translation("A field containing a JSON-LD schema."),
 *   default_widget = "jsonld_default",
 *   default_formatter = "jsonld_head",
 *   no_ui = TRUE,
 * )
 *
 * @property array $overrides
 *   The overrides for the schema properties that were configured on the bundle
 *   entity. Only properties that were explicitly overridden are represented.
 */
class JsonLdItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'overrides' => [
          'description' => 'The property overrides.',
          'type' => 'blob',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['processed'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Processed value'))
      ->setDescription(new TranslatableMarkup('The full JSON LD schema after processing tokens and filling in pre-configured data.'))
      ->setInternal(FALSE)
      ->setComputed(TRUE)
      ->setClass(JsonLdProcessed::class)
      ->setSetting('schema_bundle_source', 'schema_bundle')
      ->setSetting('schema_overrides', 'overrides');

    $properties['schema_bundle'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Schema bundle'))
      ->setComputed(TRUE)
      ->setClass(SchemaBundle::class);

    $properties['overrides'] = DataDefinition::create('schema_overrides')
      ->setLabel(new TranslatableMarkup('Overrides'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {
    // Recalculate the processed JSON-LD.
    $this->writePropertyValue('processed', NULL);
    parent::onChange($property_name, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return ['overrides' => []];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // This field, once attached, always outputs the schema data, even if only
    // the defaults; i.e.: Without any overrides.
    return FALSE;
  }

}
