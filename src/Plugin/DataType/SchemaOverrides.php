<?php

namespace Drupal\json_ld_schema_ui\Plugin\DataType;

use Drupal\Core\TypedData\TypedData;

/**
 * Provides a data type for storing overrides.
 *
 * @DataType(
 *   id = "schema_overrides",
 *   label = @Translation("Schema overrides"),
 *   description = @Translation("Overrides to the schema properties configured on the bundle entity"),
 * )
 *
 * @internal
 */
class SchemaOverrides extends TypedData {

  /**
   * The schema overrides array.
   *
   * The keys are the schema types (classes) that need to be overridden and the
   * values are associative arrays where the keys are the property paths and the
   * values contain the overridden value.
   *
   * @var string[][]
   */
  protected $value;

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if (!is_array($value)) {
      throw new \InvalidArgumentException(sprintf('Value assigned to "%s" is not an array', $this->getName()));
    }

    foreach ($value as $schema_type => $overrides) {
      // @todo Validate schema types here.
      foreach ($overrides as $property_path => $override) {
        // @todo Validate property path here.
      }
    }

    parent::setValue($value, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    $this->setValue([], $notify);
    return $this;
  }

}
