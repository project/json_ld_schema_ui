<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

/**
 * Provides an interface for schema inspectors.
 */
interface SchemaDataInterface {

  /**
   * Get the root type (class) ID.
   *
   * @return string
   *   The root type (class) ID.
   */
  public function getRootId();

  /**
   * Get the schema ID for a given label.
   *
   * This allows you to get the fully qualified schema ID for any data type,
   * type (class), property or property option by passing in its label.
   *
   * @param string $label
   *   The schema label.
   *
   * @return string
   *   The schema ID.
   */
  public function getSchemaId($label);

  /**
   * Get the schema label for a given ID.
   *
   * This allows you to get the schema label for any data type, type (class),
   * property or property option by passing in its ID.
   *
   * @param string $id
   *   The schema ID.
   *
   * @return string
   *   The schema label.
   */
  public function getSchemaLabel($id);

  /**
   * Get the property values for a given property ID.
   *
   * This looks at the passed in property ID and fetches all of the valid values
   * for said property. It maps these into a list of arrays where the outer keys
   * are: 'data_type', 'type' and 'enumeration'.
   *
   * @param string $id
   *   The schema property ID.
   *
   * @return string[][]
   *   The valid schema property values.
   */
  public function getPropertyValues($id);

  /**
   * Get the descendant paths for a given type (class) ID.
   *
   * This looks at all of the descendants of the passed in type and returns an
   * array of flattened routes to each descendant. So given the following
   * hierarchy:
   * - A
   *   - B
   *   - C
   *     - D
   *   - E
   *     - F.
   *
   * The result would be:
   * - B
   * - C
   * - C, D
   * - E
   * - E, F
   *
   * Or when $include_root is set to TRUE:
   * - A
   * - A, B
   * - A, C
   * - A, C, D
   * - A, E
   * - A, E, F
   *
   * @param string $id
   *   The schema type (class) ID.
   * @param bool $include_root
   *   (optional) Whether to include the passed in ID in the paths. Defaults to
   *   FALSE.
   *
   * @return string[][]
   *   The descendant paths.
   */
  public function getDescendantPaths($id, $include_root = FALSE);

  /**
   * Get all possible properties values for a given schema type (class).
   *
   * This will get all of the properties for the passed in class, followed by
   * all of the properties for each ancestor in ascending order.
   *
   * @param string $id
   *   The schema type (class) ID.
   *
   * @return string[]
   *   The schema property IDs, keyed by the type (class) they belong to.
   */
  public function getProperties($id);

  /**
   * Gets the Enumeration options as ID - label pairs.
   *
   * @param string $id
   *   The schema type (class) ID of the Enumeration subtype.
   *
   * @return string[]
   *   The possible values as ID - label pairs.
   */
  public function getEnumOptions($id);

}
