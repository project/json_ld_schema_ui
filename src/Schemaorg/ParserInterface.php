<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

/**
 * Provides an interface for schema parsers.
 */
interface ParserInterface {

  /**
   * Fetches the schema root.
   *
   * @return \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\TypeInterface
   *   The schema root.
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\ParseException
   */
  public function getRoot();

  /**
   * Fetches a list of schema data types.
   *
   * @return \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\DataTypeInterface[]
   *   A list of schema data types.
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\ParseException
   */
  public function getDataTypes();

  /**
   * Fetches a list of schema types (classes).
   *
   * @return \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\TypeInterface[]
   *   A list of schema types (classes).
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\ParseException
   */
  public function getClasses();

  /**
   * Fetches a list of schema properties.
   *
   * @return \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\PropertyInterface[]
   *   A list of schema properties.
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\ParseException
   */
  public function getProperties();

  /**
   * Fetches a list of schema property options.
   *
   * @return \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\PropertyOptionInterface[]
   *   A list of schema propert options.
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\ParseException
   */
  public function getPropertyOptions();

}
