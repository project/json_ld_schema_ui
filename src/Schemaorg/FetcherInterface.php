<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

/**
 * Provides an interface for schema fetchers.
 */
interface FetcherInterface {

  /**
   * Fetches the schemas.
   *
   * @return string[]|\Generator
   *   A list of raw schema strings.
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\FetchException
   */
  public function fetch();

}
