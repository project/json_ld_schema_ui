<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Represents a single schema data type.
 */
class DataType extends GraphNodeBase implements DataTypeInterface {

  /**
   * The parent IDs, if any.
   *
   * @var string[]
   */
  protected $parentIds = [];

  /**
   * The child IDs, if any.
   *
   * @var string[]
   */
  protected $childIds = [];

  /**
   * {@inheritdoc}
   */
  public static function fromElement(\stdClass $element) {
    /** @var static $instance */
    $instance = parent::fromElement($element);

    if (isset($element->{'rdfs:subClassOf'})) {
      $parents = $element->{'rdfs:subClassOf'};
      if (is_object($parents)) {
        $parents = [$parents];
      }
      foreach ($parents as $parent) {
        $instance->parentIds[] = $parent->{'@id'};
      }
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function fromType(TypeInterface $type) {
    $instance = new static($type->id(), $type->label(), $type->description());
    $instance->parentIds = $type->parentIds();
    $instance->childIds = $type->childIds();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function parentIds() {
    return $this->parentIds;
  }

  /**
   * {@inheritdoc}
   */
  public function childIds() {
    return $this->childIds;
  }

  /**
   * {@inheritdoc}
   */
  public function registerChildId($id) {
    $this->childIds[] = $id;
  }

}
