<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Defines the interface for schema properties.
 */
interface PropertyInterface extends GraphNodeInterface {

  /**
   * Gets the domain IDs, if any.
   *
   * Deprecated properties could have no domains.
   *
   * @return string[]
   *   The domain IDs.
   */
  public function domainIds();

  /**
   * Gets the range IDs, if any.
   *
   * Deprecated properties could have no ranges.
   *
   * @return string[]
   *   The range IDs.
   */
  public function rangeIds();

}
