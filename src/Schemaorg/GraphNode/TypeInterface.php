<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Defines the interface for schema types (classes).
 */
interface TypeInterface extends GraphNodeInterface {

  /**
   * Gets the parent type IDs, if any.
   *
   * @return string[]
   *   The parent type IDs.
   */
  public function parentIds();

  /**
   * Gets the child type IDs, if any.
   *
   * @return string[]
   *   The child type IDs.
   */
  public function childIds();

  /**
   * Registers a type class ID.
   *
   * @param string $id
   *   The child type ID.
   */
  public function registerChildId($id);

  /**
   * Gets the property IDs, if any.
   *
   * @return string[]
   *   The property IDs.
   */
  public function propertyIds();

  /**
   * Registers a property ID.
   *
   * @param string $id
   *   The property ID.
   */
  public function registerPropertyId($id);

  /**
   * Find out whether this is an Enumeration type.
   *
   * Enumeration types cannot live on their own, but are instead used to group
   * several enumeration options together in the overall Schema.org graph.
   *
   * @return bool
   *   Whether this is an Enumeration type.
   */
  public function isEnumeration();

  /**
   * Gets the enumeration option IDs, if any.
   *
   * @return string[]
   *   The enumeration option IDs.
   */
  public function enumerationOptionIds();

  /**
   * Registers an enumeration option ID.
   *
   * @param string $id
   *   The enumeration option ID.
   */
  public function registerEnumerationOptionId($id);

}
