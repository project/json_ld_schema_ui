<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Defines the interface for schema property options.
 */
interface PropertyOptionInterface extends GraphNodeInterface {

  /**
   * Gets the property option's types.
   *
   * This is often a subtype of Enumeration.
   *
   * @return string[]
   *   The property option's types.
   */
  public function optionTypes();

}
