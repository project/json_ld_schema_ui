<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Represents a single schema property option.
 */
class PropertyOption extends GraphNodeBase implements PropertyOptionInterface {

  /**
   * The type for the property option.
   *
   * @var string[]
   */
  protected $optionTypes = [];

  /**
   * {@inheritdoc}
   */
  public static function fromElement(\stdClass $element) {
    /** @var static $instance */
    $instance = parent::fromElement($element);
    $instance->optionTypes = (array) $element->{'@type'};
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function optionTypes() {
    return $this->optionTypes;
  }

}
