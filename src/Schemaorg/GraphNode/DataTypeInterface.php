<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Defines the interface for schema data type.
 */
interface DataTypeInterface extends GraphNodeInterface {

  /**
   * Creates a data type from a type (class).
   *
   * @param \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\TypeInterface $type
   *   The schema type (class) to convert into a data type.
   *
   * @return static
   */
  public static function fromType(TypeInterface $type);

  /**
   * Gets the parent data type IDs, if any.
   *
   * @return string[]
   *   The parent data type IDs.
   */
  public function parentIds();

  /**
   * Gets the child data type IDs, if any.
   *
   * @return string[]
   *   The child data type IDs.
   */
  public function childIds();

  /**
   * Registers a data type class ID.
   *
   * @param string $id
   *   The child data type ID.
   */
  public function registerChildId($id);

}
