<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Defines the common interface for schema graph nodes.
 */
interface GraphNodeInterface {

  /**
   * Constructs a new GraphNodeInterface from a schema.org element.
   *
   * @param object $element
   *   The element as retrieved from schema.org.
   *
   * @return static
   *   The GraphNodeInterface instance.
   */
  public static function fromElement(\stdClass $element);

  /**
   * Gets the schema class ID.
   *
   * @return string
   *   The ID.
   */
  public function id();

  /**
   * Gets the schema class label.
   *
   * @return string
   *   The label.
   */
  public function label();

  /**
   * Gets the schema class description.
   *
   * @return string
   *   The description.
   */
  public function description();

}
