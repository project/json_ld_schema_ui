<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Represents a single schema property.
 */
class Property extends GraphNodeBase implements PropertyInterface {

  /**
   * The domain IDs, if any.
   *
   * @var string[]
   */
  protected $domainIds = [];

  /**
   * The domain IDs, if any.
   *
   * @var string[]
   */
  protected $rangeIds = [];

  /**
   * {@inheritdoc}
   */
  public static function fromElement(\stdClass $element) {
    /** @var static $instance */
    $instance = parent::fromElement($element);

    // @todo Exception when not set?
    if (isset($element->{'schema:domainIncludes'})) {
      $domains = $element->{'schema:domainIncludes'};
      if (is_object($domains)) {
        $domains = [$domains];
      }
      foreach ($domains as $domain) {
        $instance->domainIds[] = $domain->{'@id'};
      }
    }

    // @todo Exception when not set?
    if (isset($element->{'schema:rangeIncludes'})) {
      $ranges = $element->{'schema:rangeIncludes'};
      if (is_object($ranges)) {
        $ranges = [$ranges];
      }
      foreach ($ranges as $range) {
        $instance->rangeIds[] = $range->{'@id'};
      }
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function domainIds() {
    return $this->domainIds;
  }

  /**
   * {@inheritdoc}
   */
  public function rangeIds() {
    return $this->rangeIds;
  }

}
