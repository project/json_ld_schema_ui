<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Represents a single schema graph node.
 */
abstract class GraphNodeBase implements GraphNodeInterface {

  /**
   * The graph node ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The graph node label.
   *
   * @var string
   */
  protected $label;

  /**
   * The graph node description.
   *
   * @var string
   */
  protected $description;

  /**
   * Constructs a new GraphNodeBase object.
   *
   * @param string $id
   *   The schema ID.
   * @param string $label
   *   The schema label.
   * @param string $description
   *   The schema description.
   */
  public function __construct($id, $label, $description) {
    $this->id = $id;
    $this->label = $label;
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public static function fromElement(\stdClass $element) {
    $id = $element->{'@id'};
    $label = $element->{'rdfs:label'};
    $description = $element->{'rdfs:comment'};

    if (is_object($label)) {
      if (isset($label->{'@value'})) {
        $label = $label->{'@value'};
      }
      else {
        throw new \Exception(sprintf('Could not determine label for %s', $id));
      }
    }

    return new static($id, $label, $description);
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->description;
  }

}
