<?php

namespace Drupal\json_ld_schema_ui\Schemaorg\GraphNode;

/**
 * Represents a single schema type (class).
 */
class Type extends GraphNodeBase implements TypeInterface {

  /**
   * The parent IDs, if any.
   *
   * @var string[]
   */
  protected $parentIds = [];

  /**
   * The child IDs, if any.
   *
   * @var string[]
   */
  protected $childIds = [];

  /**
   * The property IDs, if any.
   *
   * @var string[]
   */
  protected $propertyIds = [];

  /**
   * Whether this type is an Enumeration.
   *
   * @var bool
   */
  protected $isEnumeration = FALSE;

  /**
   * The enumeration option IDs, if any.
   *
   * @var string[]
   */
  protected $enumerationOptionIds = [];

  /**
   * {@inheritdoc}
   */
  public static function fromElement(\stdClass $element) {
    /** @var static $instance */
    $instance = parent::fromElement($element);

    if (isset($element->{'rdfs:subClassOf'})) {
      $parents = $element->{'rdfs:subClassOf'};
      if (is_object($parents)) {
        $parents = [$parents];
      }
      foreach ($parents as $parent) {
        $instance->parentIds[] = $parent->{'@id'};
      }
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function parentIds() {
    return $this->parentIds;
  }

  /**
   * {@inheritdoc}
   */
  public function childIds() {
    return $this->childIds;
  }

  /**
   * {@inheritdoc}
   */
  public function registerChildId($id) {
    $this->childIds[] = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function propertyIds() {
    return $this->propertyIds;
  }

  /**
   * {@inheritdoc}
   */
  public function registerPropertyId($id) {
    $this->propertyIds[] = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnumeration() {
    return $this->isEnumeration;
  }

  /**
   * {@inheritdoc}
   */
  public function enumerationOptionIds() {
    return $this->enumerationOptionIds;
  }

  /**
   * {@inheritdoc}
   */
  public function registerEnumerationOptionId($id) {
    $this->isEnumeration = TRUE;
    $this->enumerationOptionIds[] = $id;
  }

}
