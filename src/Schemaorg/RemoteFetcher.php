<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Fetches schema via HTTP.
 */
class RemoteFetcher implements FetcherInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a remote fetcher.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch() {
    $config = $this->configFactory->get('json_ld_schema_ui.settings');

    // The schema is fetched during module installation when the configuration
    // has not been properly installed. The parser will catch the exception.
    if ($config->isNew()) {
      throw new FetchException('Invalid schema URI configuration');
    }

    foreach ($config->get('schema.sources') as $source) {
      try {
        $response = $this->httpClient->request('GET', $source);
      }
      catch (GuzzleException $exception) {
        throw new FetchException('Error fetching the schema: ' . $exception->getMessage(), $exception->getCode(), $exception);
      }
      yield (string) $response->getBody();
    }
  }

}
