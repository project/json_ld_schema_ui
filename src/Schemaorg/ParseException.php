<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

/**
 * An exception thrown when the schema cannot be parsed.
 */
class ParseException extends \RuntimeException {
}
