<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

/**
 * Provides a class for exceptions during schema fetching.
 */
class FetchException extends \RuntimeException {
}
