<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

use Drupal\json_ld_schema_ui\Schemaorg\GraphNode\DataType;
use Drupal\json_ld_schema_ui\Schemaorg\GraphNode\Property;
use Drupal\json_ld_schema_ui\Schemaorg\GraphNode\PropertyOption;
use Drupal\json_ld_schema_ui\Schemaorg\GraphNode\Type;
use Drupal\json_ld_schema_ui\Schemaorg\GraphNode\TypeInterface;

/**
 * Provides a schema parser.
 */
class Parser implements ParserInterface {

  /**
   * The schema fetcher.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\FetcherInterface
   */
  protected $fetcher;

  /**
   * Whether the parser has already run.
   *
   * @var bool
   */
  private $parsed = FALSE;

  /**
   * The schema data types.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\DataTypeInterface[]
   */
  protected $dataTypes = [];

  /**
   * The schema classes.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\TypeInterface[]
   */
  protected $classes = [];

  /**
   * The schema properties.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\PropertyInterface[]
   */
  protected $properties = [];

  /**
   * The schema property options.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\GraphNode\PropertyOptionInterface[]
   */
  protected $propertyOptions = [];

  /**
   * Constructs a schema parser.
   *
   * @param \Drupal\json_ld_schema_ui\Schemaorg\FetcherInterface $fetcher
   *   The schema fetcher.
   */
  public function __construct(FetcherInterface $fetcher) {
    $this->fetcher = $fetcher;
  }

  /**
   * Parses the entire schema.
   *
   * @throws \Drupal\json_ld_schema_ui\Schemaorg\ParseException
   */
  protected function parse() {
    if ($this->parsed) {
      return;
    }

    try {
      // Keep track of sources for now but don't do anything with them as not
      // all elements have them (yet?). Mainly useful for debugging.
      $sources = [];

      foreach ($this->fetcher->fetch() as $contents) {
        $raw_data = json_decode($contents);

        if (json_last_error() !== JSON_ERROR_NONE) {
          throw new ParseException('Error parsing the schema: ' . json_last_error_msg());
        }

        if (!isset($raw_data->{'@graph'}) || !is_array($raw_data->{'@graph'})) {
          throw new ParseException('Missing schema graph');
        }

        foreach ($raw_data->{'@graph'} as $i => $element) {
          if (!is_object($element) || !$this->hasStringProperty($element, '@id')) {
            throw new ParseException(sprintf('Invalid schema element at index %d', $i));
          }
          $id = $element->{'@id'};

          if (isset($element->{'schema:isPartOf'})) {
            $source = $element->{'schema:isPartOf'}->{'@id'};
            $sources += [$source => 1];
            $sources[$source]++;
          }

          if (is_array($element->{'@type'})) {
            // @todo This could be more robust in case URL changes.
            if (in_array('schema:DataType', $element->{'@type'})) {
              $this->dataTypes[$id] = DataType::fromElement($element);
            }
            else {
              $this->propertyOptions[$id] = PropertyOption::fromElement($element);
            }
            continue;
          }

          if (!is_string($element->{'@type'})) {
            throw new ParseException(sprintf('Invalid type for id %s', $id));
          }

          switch ($element->{'@type'}) {
            case 'rdfs:Class':
              $this->classes[$id] = Type::fromElement($element);
              break;

            case 'rdf:Property':
              $this->properties[$id] = Property::fromElement($element);
              break;

            default:
              $this->propertyOptions[$id] = PropertyOption::fromElement($element);
          }
        }
      }
    }
    catch (FetchException $exception) {
      // Fetcher throws exception when config isn't installed yet. Bail out.
      return;
    }

    if (empty($this->dataTypes)) {
      throw new ParseException('No data types found in schema graph');
    }

    if (empty($this->classes)) {
      throw new ParseException('No classes found in schema graph');
    }

    if (empty($this->properties)) {
      throw new ParseException('No properties found in schema graph');
    }

    // Set type (class) children and move data types that were added to the list
    // of types because they resemble regular types. This happens to subtypes of
    // the main data types, such as Float (Number).
    $convert_to_data_type = [];
    foreach ($this->classes as $id => $class) {
      // DataType itself is not a valid data type.
      if ($class->label() === 'DataType') {
        unset($this->classes[$id]);
        continue;
      }

      foreach ($class->parentIds() as $parent_id) {
        if (isset($this->classes[$parent_id])) {
          $this->classes[$parent_id]->registerChildId($id);
        }
        // Some types (classes) are actually DataType descendants and need to be
        // converted after this loop has finished assigning parents.
        elseif (isset($this->dataTypes[$parent_id])) {
          $this->dataTypes[$parent_id]->registerChildId($id);
          $convert_to_data_type[] = $id;
        }
      }
    }

    // Convert the data type classes into data types and make sure we do the
    // same for all of their descendants.
    foreach ($convert_to_data_type as $id) {
      $classes = $this->classes;
      $find_descendants = function ($ids) use (&$find_descendants, $classes) {
        $descendant_ids = [];
        foreach ($ids as $id) {
          $descendant_ids[] = $find_descendants($classes[$id]->childIds());
        }
        return array_merge($ids, ...$descendant_ids);
      };

      foreach ($find_descendants([$id]) as $descendant_id) {
        $class = $this->classes[$descendant_id];
        unset($this->classes[$descendant_id]);
        $this->dataTypes[$descendant_id] = DataType::fromType($class);
      }
    }

    foreach ($this->properties as $property) {
      foreach ($property->domainIds() as $domain_id) {
        // Properties may list domains that we did not parse.
        if (isset($this->classes[$domain_id])) {
          $this->classes[$domain_id]->registerPropertyId($property->id());
        }
      }
    }

    foreach ($this->propertyOptions as $option) {
      foreach ($option->optionTypes() as $option_type) {
        // DataType can be Enumeration too (e.g.: Boolean).
        if (isset($this->dataTypes[$option_type])) {
          // Do nothing for now.
        }
        elseif (isset($this->classes[$option_type])) {
          $optionType = $this->classes[$option_type];
          $optionType->registerEnumerationOptionId($option->id());
        }
      }
    }

    ksort($this->dataTypes);
    ksort($this->classes);
    ksort($this->properties);
    ksort($this->propertyOptions);

    $this->parsed = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoot() {
    $this->parse();

    // We consider any class that is not a subclass of another class a root.
    $root_classes = array_filter($this->classes, function (TypeInterface $class) {
      return count($class->parentIds()) === 0;
    });

    if (count($root_classes) > 1) {
      throw new ParseException('No definitive root could be determined');
    }

    return reset($root_classes);
  }

  /**
   * {@inheritdoc}
   */
  public function getDataTypes() {
    $this->parse();
    return $this->dataTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function getClasses() {
    $this->parse();
    return $this->classes;
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties() {
    $this->parse();
    return $this->properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyOptions() {
    $this->parse();
    return $this->propertyOptions;
  }

  /**
   * Checks whether or not an object has a given string property.
   *
   * @param object $object
   *   The object.
   * @param string $property
   *   The property name.
   *
   * @return bool
   *   TRUE if the object has a string property with the given name; FALSE
   *   otherwise.
   */
  protected function hasStringProperty($object, $property) {
    return isset($object->{$property}) && is_string($object->{$property});
  }

}
