<?php

namespace Drupal\json_ld_schema_ui\Schemaorg;

/**
 * Provides a value object that stores the schema data in state.
 *
 * The chosen data structure is an attempt to provide fast and direct access to
 * the information required by the different user interface elements while still
 * avoiding redundancy. It is not fully normalized because both ancestor and
 * descendant information is stored, even though one can be parsed into the
 * other. On the other hand, it avoids storing both the children and the
 * descendant list in favor of a single descendant structure which can be easily
 * transformed into either. It also does not store inherited properties so that
 * those have to be resolved at runtime.
 */
class SchemaData implements SchemaDataInterface {

  /**
   * The schema parser.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\ParserInterface
   */
  protected $parser;

  /**
   * A mapping converting all known labels to IDs.
   *
   * @var string[]
   */
  protected $labelToIdMap;

  /**
   * A mapping converting all known IDs to labels.
   *
   * @var string[]
   */
  protected $idToLabelMap;

  /**
   * The list of properties for each schema type.
   *
   * @var array[]
   */
  protected $properties;

  /**
   * The descendant path array.
   *
   * @var array[]
   */
  protected $descendantPaths;

  /**
   * Constructs a schema data object.
   *
   * @param \Drupal\json_ld_schema_ui\Schemaorg\ParserInterface $parser
   *   The parser service.
   */
  public function __construct(ParserInterface $parser) {
    $this->parser = $parser;
    foreach ($this->parser->getDataTypes() as $id => $graph_node) {
      $this->labelToIdMap[$graph_node->label()] = $id;
    }
    foreach ($this->parser->getClasses() as $id => $graph_node) {
      $this->labelToIdMap[$graph_node->label()] = $id;
    }
    foreach ($this->parser->getProperties() as $id => $graph_node) {
      $this->labelToIdMap[$graph_node->label()] = $id;
    }
    foreach ($this->parser->getPropertyOptions() as $id => $graph_node) {
      $this->labelToIdMap[$graph_node->label()] = $id;
    }
    $this->idToLabelMap = array_flip($this->labelToIdMap);
  }

  /**
   * {@inheritdoc}
   */
  public function getRootId() {
    return $this->parser->getRoot()->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaId($label) {
    return $this->labelToIdMap[$label];
  }

  /**
   * {@inheritdoc}
   */
  public function getSchemaLabel($id) {
    return $this->idToLabelMap[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyValues($id) {
    $data_types = $this->parser->getDataTypes();
    $classes = $this->parser->getClasses();

    $values = [];
    foreach ($this->parser->getProperties()[$id]->rangeIds() as $range_id) {
      if (isset($data_types[$range_id])) {
        $values['data_type'][] = $range_id;
      }
      elseif (isset($classes[$range_id])) {
        if ($classes[$range_id]->isEnumeration()) {
          $values['enumeration'][] = $range_id;
        }
        else {
          $values['type'][] = $range_id;
        }
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescendantPaths($id, $include_root = FALSE) {
    $root_key = $include_root ? 'with_root' : 'without_root';

    if (!isset($this->descendantPaths[$id])) {
      $classes = $this->parser->getClasses();

      $descendants = [];
      foreach ($classes[$id]->childIds() as $child_id) {
        $descendants[] = [$child_id];
        foreach ($this->getDescendantPaths($child_id) as $child_path) {
          $descendants[] = array_merge([$child_id], $child_path);
        }
      }

      $this->descendantPaths[$id]['without_root'] = $descendants;
      $this->descendantPaths[$id]['with_root'] = array_merge([[$id]], array_map(function ($item) use ($id) {
        return array_merge([$id], $item);
      }, $descendants));
    }

    return $this->descendantPaths[$id][$root_key];
  }

  /**
   * {@inheritdoc}
   */
  public function getProperties($id) {
    if (!isset($this->properties[$id])) {
      $classes = $this->parser->getClasses();
      $class = $classes[$id];
      $properties[$id] = $class->propertyIds();
      sort($properties[$id]);

      $parent_properties = [];
      foreach ($class->parentIds() as $parent_id) {
        $parent_properties[] = $this->getProperties($parent_id);
      }
      $parent_properties[] = $properties;
      $properties = array_merge(...$parent_properties);

      $this->properties[$id] = $properties;
    }
    return $this->properties[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function getEnumOptions($id) {
    $classes = $this->parser->getClasses();
    if (!isset($classes[$id]) || !$classes[$id]->isEnumeration()) {
      throw new \InvalidArgumentException(sprintf('Invalid or missing type for ID %s', $id));
    }
    $property_options = $this->parser->getPropertyOptions();

    $options = [];
    foreach ($classes[$id]->enumerationOptionIds() as $option_id) {
      $options[$option_id] = $property_options[$option_id]->label();
    }
    return $options;
  }

}
