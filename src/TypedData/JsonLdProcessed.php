<?php

namespace Drupal\json_ld_schema_ui\TypedData;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedData;

/**
 * Provides a computed property for processing JSON-LD by replacing tokens.
 *
 * Required settings (below the definition's 'settings' key) are:
 * - schema_bundle_source: The text property containing the bundle property of
 *   the schema config entities to be overridden.
 * - schema_overrides: The 'schema_overrides' datatype property that contains
 *   the overrides for the preconfigured schema properties.
 */
class JsonLdProcessed extends TypedData implements CacheableDependencyInterface {

  /**
   * JSON-encoded processed value.
   *
   * @var string|null
   */
  protected $value = NULL;

  /**
   * The data to pass into the token replace call.
   *
   * @var array
   */
  protected $tokenData = [];

  /**
   * The options to pass into the token replace call.
   *
   * @var array
   */
  protected $tokenOptions = [];

  /**
   * The bubbleable metadata.
   *
   * @var \Drupal\Core\Render\BubbleableMetadata
   */
  protected $bubbleableMetadata;

  /**
   * {@inheritdoc}
   */
  public static function createInstance($definition, $name = NULL, TraversableTypedDataInterface $parent = NULL) {
    if ($definition->getSetting('schema_bundle_source') === NULL) {
      throw new \InvalidArgumentException("The definition's 'schema_bundle_source' setting is missing.");
    }
    if ($definition->getSetting('schema_overrides') === NULL) {
      throw new \InvalidArgumentException("The definition's 'schema_overrides' setting is missing.");
    }

    /** @var static $instance */
    $instance = parent::createInstance($definition, $name, $parent);
    $instance->bubbleableMetadata = new BubbleableMetadata();
    $instance->bubbleableMetadata->addCacheTags(
      \Drupal::entityTypeManager()
        ->getDefinition('schema_content_settings')
        ->getListCacheTags()
    );
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->value !== NULL) {
      return $this->value;
    }

    $item = $this->getParent();
    $schema_bundle = $item->{($this->definition->getSetting('schema_bundle_source'))};

    /** @var \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings[] $schema_configs */
    $schema_configs = \Drupal::entityTypeManager()
      ->getStorage('schema_content_settings')
      ->loadByProperties(['bundle' => $schema_bundle]);

    if (count($schema_configs) == 0) {
      $this->value = $this->encode([]);
      return $this->value;
    }

    /** @var \Drupal\json_ld_schema_ui\Plugin\Field\FieldType\JsonLdItem $item */
    $entity = $item->getEntity();
    $this->tokenData = [$entity->getEntityTypeId() => $entity];
    $this->tokenOptions = ['langcode' => $item->getLangcode()];

    $context_uri = rtrim(\Drupal::config('json_ld_schema_ui.settings')->get('schema.base_uri'), '/') . '/';
    $full_data = ['@context' => $context_uri, '@graph' => []];

    $schema_overrides = $item->{($this->definition->getSetting('schema_overrides'))};
    foreach ($schema_configs as $schema_config) {
      $schema_type = $schema_config->getSchemaType();
      $overrides = $schema_overrides[$schema_type] ?? [];
      $full_data['@graph'][] = ['@type' => $schema_type] + $this->processProperties($schema_config->getSchemaProperties(), $overrides);
    }

    // Simplify into a single type if possible.
    if (count($full_data['@graph']) == 1) {
      $full_data = ['@context' => $context_uri] + reset($full_data['@graph']);
    }

    return $this->value = $this->encode($full_data);
  }

  /**
   * Processes the properties into a valid structure for the schema.org JSON.
   *
   * @param array $properties
   *   The properties.
   * @param array $overrides
   *   The overrides stored in the field.
   * @param array $property_path
   *   (optional) The active property path, used in recursion.
   *
   * @return array
   *   The processed properties.
   */
  protected function processProperties(array $properties, array $overrides, array $property_path = []) {
    $processed = [];

    foreach ($properties as $label => $property) {
      $current_path = array_merge($property_path, [$label]);

      if (empty($property['properties'])) {
        $override_path = implode('|', $current_path);

        $property_value = $property['default_value'];
        if (isset($overrides[$override_path])) {
          $property_value = $overrides[$override_path];
        }

        $processed[$label] = \Drupal::token()->replace(
          $property_value,
          $this->tokenData,
          $this->tokenOptions,
          $this->bubbleableMetadata
        );
      }
      else {
        $processed[$label] = $this->processProperties($property['properties'], $overrides, $current_path);
      }
    }

    return $processed;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->bubbleableMetadata->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->bubbleableMetadata->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->bubbleableMetadata->getCacheMaxAge();
  }

  /**
   * Encodes a value for storage.
   *
   * @param array $value
   *   The value to encode.
   *
   * @return string
   *   The encoded value.
   */
  public function encode(array $value) {
    return json_encode($value, JSON_UNESCAPED_UNICODE);
  }

}
