<?php

namespace Drupal\json_ld_schema_ui\TypedData;

use Drupal\Core\TypedData\TypedData;

/**
 * Provides a computed property for the schema_content_settings bundle property.
 */
class SchemaBundle extends TypedData {

  /**
   * The bundle property for schema_content_settings entities.
   *
   * @var string|null
   */
  protected $value = NULL;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->value !== NULL) {
      return $this->value;
    }
    $entity = $this->getParent()->getEntity();
    return $this->value = $entity->getEntityTypeId() . '.' . $entity->bundle();
  }

}
