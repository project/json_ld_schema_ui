<?php

namespace Drupal\json_ld_schema_ui\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings;
use Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds schema types to existing entities.
 */
class EntitySchemaConfigurationForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The schema data service.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface
   */
  protected $schemaData;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a EntitySchemaConfigurationForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface $schema_data
   *   The schema data service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, SchemaDataInterface $schema_data, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->schemaData = $schema_data;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('json_ld_schema_ui.schema'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo Implement base form ID.
   */
  public function getFormId() {
    return 'json_ld_schema_ui_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $bundle_entity_type = NULL, RouteMatchInterface $route_match = NULL) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle_entity */
    $bundle_entity = $route_match->getParameter($bundle_entity_type);
    $form_state->set('target_entity_type', $target_entity_type = $bundle_entity->getEntityType()->getBundleOf());
    $form_state->set('target_bundle', $target_bundle = $bundle_entity->id());

    /** @var \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings[] $schema_settings */
    $schema_settings = $this->entityTypeManager
      ->getStorage('schema_content_settings')
      ->loadByProperties(['bundle' => "$target_entity_type.$target_bundle"]);

    // We have a lot of nested forms so this is important.
    $form['#tree'] = TRUE;

    $form['add_schema'] = $this->buildAddSchemaForm();
    $schema_parents = [];
    foreach ($schema_settings as $id => $schema_setting) {
      $php_safe_id = str_replace('.', '_', $schema_setting->id());
      $form[$php_safe_id] = $this->buildSchemaForm($schema_setting, $route_match);
      $schema_parents[] = [$php_safe_id];
    }

    if (count($schema_settings)) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save properties'),
        '#button_type' => 'primary',
        '#limit_validation_errors' => $schema_parents,
        // Required for #limit_validation_errors to work on form-wide submit.
        '#submit' => ['::submitForm'],
      ];
    }

    if ($this->moduleHandler->moduleExists('token')) {
      $form['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => [$target_entity_type],
      ];
    }

    return $form;
  }

  /**
   * Builds the add schema form.
   *
   * @return array
   *   The form structure.
   */
  protected function buildAddSchemaForm() {
    $form = [
      '#type' => 'details',
      '#title' => $this->t('Add a new schema'),
      '#open' => TRUE,
      '#process' => ['::processSubform'],
    ];

    $options = [];
    foreach ($this->schemaData->getDescendantPaths($this->schemaData->getRootId()) as $path) {
      $label_parts = [];
      foreach ($path as $id) {
        $label_parts[] = $this->schemaData->getSchemaLabel($id);
      }
      // A single type may occur multiple time so add a fast hash to the ID.
      $options[$id . '#' . md5(json_encode($path))] = implode(' > ', $label_parts);
    }
    asort($options);

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $options,
      '#required' => TRUE,
      '#prefix' => '<div class="container-inline">',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add schema'),
      '#button_type' => 'primary',
      '#validate' => ['::validateAddSchema'],
      '#submit' => ['::submitAddSchema'],
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateAddSchema(array &$form, FormStateInterface $form_state) {
    $parents = array_slice($form_state->getTriggeringElement()['#parents'], 0, -1);
    $type = $form_state->getValue(array_merge($parents, ['type']));
    if (empty($type)) {
      return;
    }
    $type_label = $this->schemaData->getSchemaLabel(explode('#', $type)[0]);

    $config_id = implode('.', [
      $form_state->get('target_entity_type'),
      $form_state->get('target_bundle'),
      strtolower($type_label),
    ]);
    $config_id_exists = $this->entityTypeManager
      ->getStorage('schema_content_settings')
      ->getQuery()
      ->condition('id', $config_id)
      ->count()
      ->execute();

    if ($config_id_exists) {
      $form_state->setErrorByName(implode('][', $parents), $this->t('Type @type already configured.', ['@type' => $type_label]));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitAddSchema(array &$form, FormStateInterface $form_state) {
    $parents = array_slice($form_state->getTriggeringElement()['#parents'], 0, -1);
    $storage = $this->entityTypeManager->getStorage('schema_content_settings');
    $type = explode('#', $form_state->getValue(array_merge($parents, ['type'])))[0];
    $storage->save($storage->create([
      'target_entity_type' => $form_state->get('target_entity_type'),
      'target_bundle' => $form_state->get('target_bundle'),
      'schema_type' => $this->schemaData->getSchemaLabel(explode('#', $type)[0]),
    ]));
  }

  /**
   * Builds the schema form for a type.
   *
   * @param \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings $settings
   *   The configured schema settings for this type.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match, useful for building links.
   *
   * @return array
   *   The form structure.
   */
  protected function buildSchemaForm(ContentSchemaSettings $settings, RouteMatchInterface $route_match) {
    $form = [
      '#type' => 'details',
      '#title' => $this->t('Configure properties for @type', ['@type' => $settings->getSchemaType()]),
    ];

    $enabled_properties = $settings->getSchemaProperties();
    $schema_entity_id_value = ['#type' => 'value', '#value' => $settings->id()];

    // Build a table for the main property and a dedicated one for each property
    // that represents a schema.org type (class) of its own.
    $build_property_tables = function ($property_path, $properties) use (&$build_property_tables, $route_match, $settings) {
      foreach ($route_match->getRawParameters() as $key => $value) {
        $add_property_parameters[$key] = $value;
      }
      $add_property_parameters['schema_type'] = $settings->getSchemaType();
      $add_property_parameters['property_path'] = $property_path;
      $add_property_link = Link::fromTextAndUrl(
        $this->t('Add more properties'),
        Url::fromRoute(
          $route_match->getRouteName() . '.add_property',
          $add_property_parameters,
          ['query' => ['destination' => \Drupal::destination()->get()]]
        )
      );

      $unique_id_parts = array_merge(
        [$settings->getSchemaType()],
        array_slice(explode('|', $property_path), 1)
      );

      $build[$property_path] = [
        '#type' => 'details',
        '#title' => $property_path === '***root***'
        ? $this->t('Main properties')
        : $this->t('Nested properties for @path property', [
          '@path' => implode(' > ', array_slice(explode('|', $property_path), 1)),
        ]),
        '#open' => TRUE,
        '#id' => 'prop-' . implode('-', $unique_id_parts),
      ];

      $build[$property_path]['table'] = [
        '#type' => 'table',
        '#header' => [
          'label' => $this->t('Label'),
          'default_value' => $this->t('Default value'),
          'allow_override' => $this->t('Allow override'),
          'actions' => $this->t('Actions'),
        ],
        '#empty' => $this->t('There are no configured properties yet.'),
        '#footer' => [
          [
            'colspan' => 4,
            'data' => [$add_property_link],
          ],
        ],
      ];

      foreach ($properties as $label => $property) {
        $row = &$build[$property_path]['table'][$label];
        $row['label'] = ['#markup' => $label];

        $nested_unique_id_parts = array_merge($unique_id_parts, [$label]);
        $nested_property_path = $property_path . '|' . $label;
        if (empty($property['properties'])) {
          $row['default_value'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Default value'),
            '#title_display' => 'invisible',
            '#default_value' => $property['default_value'] ?: '',
            '#disabled' => $label == '@type',
          ];

          if (!empty($property['enum_type'])) {
            $row['default_value']['#type'] = 'select';
            $row['default_value']['#options'] = $this->schemaData->getEnumOptions($property['enum_type']);
          }

          if ($label == '@type') {
            $row['allow_override'] = [
              '#type' => 'value',
              '#value' => FALSE,
            ];
          }
          else {
            $delete_label = $this->t('Delete property');
            $row['allow_override'] = [
              '#type' => 'checkbox',
              '#title' => $this->t('Allow override'),
              '#title_display' => 'invisible',
              '#default_value' => $property['allow_override'] ?: FALSE,
              '#disabled' => $label == '@type',
            ];
          }
        }
        else {
          $row['default_value'] = [
            '#markup' => $this->t(
              '<em>Nested property, <a href="#@anchor">see dedicated table below</a></em>',
              ['@anchor' => 'prop-' . implode('-', $nested_unique_id_parts)]
            ),
            '#wrapper_attributes' => ['colspan' => 2],
          ];
          $delete_label = $this->t('Delete all children');

          $build += $build_property_tables($nested_property_path, $property['properties']);
        }

        if (!empty($delete_label)) {
          $row['actions']['#process'] = ['::processSubform'];
          $row['actions']['schema_entity_id'] = [
            '#type' => 'value',
            '#value' => $settings->id(),
          ];
          $row['actions']['property_path'] = [
            '#type' => 'value',
            '#value' => $nested_property_path,
          ];
          $row['actions']['submit'] = [
            '#type' => 'submit',
            '#name' => 'delete_property_' . implode('_', $nested_unique_id_parts),
            '#value' => $delete_label,
            '#submit' => ['::submitDeleteProperty'],
          ];
        }
      }

      return $build;
    };

    $form['properties'] = [];
    $form['properties'] += $build_property_tables('***root***', $enabled_properties);

    $form['actions'] = [
      '#type' => 'actions',
      '#process' => ['::processSubform'],
      'schema_entity_id' => $schema_entity_id_value,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#name' => 'delete_schema_' . $settings->getSchemaType(),
      '#value' => $this->t('Delete schema'),
      '#submit' => ['::submitDeleteSchema'],
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitDeleteSchema(array &$form, FormStateInterface $form_state) {
    $parents = array_slice($form_state->getTriggeringElement()['#parents'], 0, -1);
    $storage = $this->entityTypeManager->getStorage('schema_content_settings');
    $storage->delete([$storage->load($form_state->getValue(array_merge($parents, ['schema_entity_id'])))]);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitDeleteProperty(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue(array_slice($form_state->getTriggeringElement()['#parents'], 0, -1));

    $storage = $this->entityTypeManager->getStorage('schema_content_settings');
    $schema_entity = $storage->load($values['schema_entity_id']);
    $schema_properties = $schema_entity->get('schema_properties');
    $property_path = explode('|', $values['property_path']);

    // Traverse the property path so we know which one to remove.
    $active_properties = &$schema_properties;
    foreach (array_slice($property_path, 1, -1) as $property) {
      $active_properties = &$active_properties[$property]['properties'];
    }
    $property = end($property_path);

    unset($active_properties[$property]);
    $schema_entity->set('schema_properties', $schema_properties);
    $storage->save($schema_entity);
  }

  /**
   * Processes a subform to add limited validation.
   */
  public function processSubform(&$element, FormStateInterface $form_state, &$complete_form) {
    foreach (Element::getVisibleChildren($element) as $child_key) {
      $child = &$element[$child_key];
      if (!empty($child['#type']) && $child['#type'] === 'submit') {
        $child['#limit_validation_errors'] = [$element['#parents']];
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $target_entity_type = $form_state->get('target_entity_type');
    $target_bundle = $form_state->get('target_bundle');

    /** @var \Drupal\json_ld_schema_ui\Entity\ContentSchemaSettings[] $schema_settings */
    $storage = $this->entityTypeManager->getStorage('schema_content_settings');
    $schema_settings = $storage->loadByProperties(['bundle' => "$target_entity_type.$target_bundle"]);

    foreach ($schema_settings as $schema_setting) {
      $php_safe_id = str_replace('.', '_', $schema_setting->id());
      $values = $form_state->getValue([$php_safe_id, 'properties']);

      $schema_properties = $schema_setting->getSchemaProperties();
      foreach ($values as $property_path => $property_values) {
        if (empty($property_values['table'])) {
          continue;
        }

        // Traverse the property path so we know where to set the values.
        $active_properties = &$schema_properties;
        foreach (array_slice(explode('|', $property_path), 1) as $property_label) {
          $active_properties = &$active_properties[$property_label]['properties'];
        }

        foreach ($property_values['table'] as $property_label => $property_settings) {
          if (!isset($property_settings['default_value'])) {
            continue;
          }

          $active_properties[$property_label]['default_value'] = $property_settings['default_value'];
          $active_properties[$property_label]['allow_override'] = (bool) $property_settings['allow_override'];
        }
      }

      $schema_setting->set('schema_properties', $schema_properties);
      $storage->save($schema_setting);
    }
  }

}
