<?php

namespace Drupal\json_ld_schema_ui\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a form for the schema configuration.
 */
class SchemaSettingsConfigForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constrcuts a schema configuration form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);

    $this->httpClient = $http_client;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['json_ld_schema_ui.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'json_ld_schema_ui_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['schema']['sources'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sources'),
      '#description' => $this->t('Schema.org keeps changing or removing the individual version data so for now the module is locked to the latest version.'),
      '#default_value' => 'https://schema.org/version/latest/schemaorg-current-https.jsonld',
      '#disabled' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('json_ld_schema_ui.settings')
      ->set('schema.base_uri', 'https://schema.org')
      ->set('schema.sources', ['https://schema.org/version/latest/schemaorg-current-https.jsonld'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
