<?php

namespace Drupal\json_ld_schema_ui\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds schema properties to existing schema types.
 */
class EntitySchemaAddPropertyForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The schema data service.
   *
   * @var \Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface
   */
  protected $schemaData;

  /**
   * Constructs a EntitySchemaConfigurationForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\json_ld_schema_ui\Schemaorg\SchemaDataInterface $schema_data
   *   The schema data service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, SchemaDataInterface $schema_data) {
    $this->entityTypeManager = $entityTypeManager;
    $this->schemaData = $schema_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('json_ld_schema_ui.schema')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @todo Implement base form ID.
   */
  public function getFormId() {
    return 'json_ld_schema_ui_form_add_property';
  }

  /**
   * Generates the title for this form.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function formTitle($schema_type = NULL, $property_path = NULL) {
    $properties = explode('|', $property_path);
    $properties[0] = $schema_type;
    return $this->t('Add property to @type', ['@type' => implode(' > ', $properties)]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $bundle_entity_type = NULL, $schema_type = NULL, $property_path = NULL, RouteMatchInterface $route_match = NULL) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle_entity */
    $bundle_entity = $route_match->getParameter($bundle_entity_type);
    $schema_entity_id = implode('.', [
      $bundle_entity->getEntityType()->getBundleOf(),
      $bundle_entity->id(),
      strtolower($schema_type),
    ]);
    $form_state->set('schema_entity_id', $schema_entity_id);
    $form_state->set('property_path', $property_path);

    $schema_entity = $this->entityTypeManager
      ->getStorage('schema_content_settings')
      ->load($schema_entity_id);

    // Follow the property path so we know what properties are set and are still
    // available to be added to the type or subtype.
    $active_type = $schema_type;
    $active_properties = $schema_entity->getSchemaProperties();
    foreach (array_slice(explode('|', $property_path), 1) as $property) {
      if (!isset($active_properties[$property]['properties']['@type']['default_value'])) {
        throw new \Exception('Invalid property path');
      }
      $active_type = $active_properties[$property]['properties']['@type']['default_value'];
      $active_properties = $active_properties[$property]['properties'];
    }

    $active_type_id = $this->schemaData->getSchemaId($active_type);

    $options = [];
    $available_properties = array_reverse($this->schemaData->getProperties($active_type_id));
    foreach ($available_properties as $type => $properties) {
      $group = $this->t('Properties for @type', ['@type' => $this->schemaData->getSchemaLabel($type)]);
      foreach ($properties as $property) {
        $property_label = $this->schemaData->getSchemaLabel($property);

        // Skip already enabled properties.
        if (isset($active_properties[$property_label])) {
          continue;
        }
        $options[$group->__toString()][$property] = $property_label;
      }
    }

    $form['property'] = [
      '#type' => 'select',
      '#title' => $this->t('Property'),
      '#options' => $options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::showPropertyOptions',
        'wrapper' => 'property-details',
      ],
    ];

    $form['property_details'] = [
      '#prefix' => '<div id="property-details">',
      '#suffix' => '</div>',
    ];
    $this->buildPropertyOptions($form['property_details'], $form_state);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add property'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Builds the property options according to what was selected.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildPropertyOptions(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['property'])) {
      return;
    }

    $property_values = $this->schemaData->getPropertyValues($values['property']);
    if (empty($property_values['type']) && empty($property_values['enumeration'])) {
      $form['info'] = ['#markup' => $this->t('<p>This property can only be added as a simple value, click the button below to do so.</p>')];
      $form['property_type'] = ['#type' => 'value', '#value' => '***simple***'];
    }
    else {
      $options = [];

      if (!empty($property_values['type'])) {
        foreach ($property_values['type'] as $value) {
          foreach ($this->schemaData->getDescendantPaths($value, TRUE) as $path) {
            $label_parts = [];
            foreach ($path as $id) {
              $label_parts[] = $this->schemaData->getSchemaLabel($id);
            }
            // A single type may occur multiple time so add a hash to the ID.
            $options[$id . '#' . md5(json_encode($path))] = implode(' > ', $label_parts);
          }
        }
        asort($options);
      }

      if (!empty($property_values['enumeration'])) {
        $enum_options = [];
        foreach ($property_values['enumeration'] as $id) {
          $enum_options[$id . '#enum'] = $this->t('Add as a options of @enum', ['@enum' => $this->schemaData->getSchemaLabel($id)]);
        }
        asort($enum_options);
        $options = $enum_options + $options;
      }

      if (!empty($property_values['data_type'])) {
        $options = ['***simple***' => $this->t('Add as a simple value')] + $options;
      }

      $form['property_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Add property as'),
        '#options' => $options,
      ];
    }
  }

  /**
   * AJAX callback.
   */
  public function showPropertyOptions(array &$form, FormStateInterface $form_state) {
    return $form['property_details'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['property_type'])) {
      $form_state->setError($form['property'], $this->t('Property was not configured completely.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('schema_content_settings');
    $schema_entity = $storage->load($form_state->get('schema_entity_id'));
    $schema_properties = $schema_entity->get('schema_properties');

    // Traverse the property path so we know where to set the new one.
    $active_properties = &$schema_properties;
    foreach (array_slice(explode('|', $form_state->get('property_path')), 1) as $property) {
      $active_properties = &$active_properties[$property]['properties'];
    }

    $property = $form_state->getValue('property');
    $property_type = $form_state->getValue('property_type');
    $property_label = $this->schemaData->getSchemaLabel($property);

    // Simple values default to empty strings that can't be overridden.
    if ($property_type == '***simple***') {
      $active_properties[$property_label] = [
        'default_value' => '',
        'allow_override' => FALSE,
      ];
    }
    // Complex values default to setting the @type property of the subtype.
    else {
      [$sub_type_id, $hash] = explode('#', $property_type);
      if ($hash == 'enum') {
        $active_properties[$property_label] = [
          'default_value' => '',
          'allow_override' => FALSE,
          'enum_type' => $sub_type_id,
        ];
      }
      else {
        $sub_type_label = $this->schemaData->getSchemaLabel($sub_type_id);
        $active_properties[$property_label]['properties']['@type'] = [
          'default_value' => $sub_type_label,
          'allow_override' => FALSE,
        ];
      }
    }

    $schema_entity->set('schema_properties', $schema_properties);
    $storage->save($schema_entity);
  }

}
