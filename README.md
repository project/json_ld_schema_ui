# Content of this file

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers

## Introduction

### Bundle configuration

This module adds a schema configuration (vertical) tab to bundle configuration
forms, for example at `/admin/structure/types/manage/article` or
`/admin/structure/block/block-content/manage/basic`, which allows one or more
schema types to be configured for entities of that bundle. For each bundle the
set of properties that should be enabled can be selected and default values can
be set for them. If the [Token](https://www.drupal.org/project/token) module is
available, a token browser will be displayed to assist in the usage of
entity-specific tokens for these default values. (Note that tokens will be
replaced regardless of whether the _Token_ module is installed or not.)

For schema properties that reference other schema types the (nested) properties
of the referenced type can be enabled and configured just like any top-level
properties. For example, for the _Movie_ schema type, the _actor_ property is a
reference to the _Person_ schema type, so that the _name_ or, for example, the
_birthdate_ property of an actor associated with a movie can be specified along
with other metadata about a movie.

In addition to the configuration in context of a specific bundle, the
configurations for all bundles can be managed at _Administration >
Configuration > Search and metadata > Content Schema Settings_
(`/admin/config/search/schemaorg/settings`).

### Entity configuration

For each bundle with one or more schema settings a field will be created to hold
the per-entity schema configuration values.

The field comes with a widget that allows selecting one of the configured schema
types and subsequently allows configuring the enabled schema properties. The
fields for the properties will be prefilled with the respective default values
that were configured for that schema type configuration.

The field provides a computed property which contains the token-replaced JSON-LD
snippet. When the entity is rendered, a formatter will take that snippet and add
it to the page head. This makes the it completely independent of the themed
output of any particular entity.

## Requirements

No special requirements.

## Recommended modules

No recommended modules.

## Installation

Install this module like any other Drupal module. Because this module uses both
the Ajax and the Javascript states API in forms, the [patch]() from the
Drupal.org issue [Display Bug when using #states (Forms API) with Ajax
Request](https://www.drupal.org/project/drupal/issues/1091852) is
required for the form to behave correctly.

## Configuration
The module provides some basic configuration which can be managed at
_Administration > Configuration > Search and metadata > Content Schema
Settings > Settings_ (`/admin/config/search/schemaorg/settings`). The default settings for the schema
should be fine for most websites. Note that the settings are currently not
properly validated, so changing them can break the functionality of the module.

If the [Select2](https://www.drupal.org/project/select2) module is installed,
you can optionally enable a Select2-based schema type configuration instead of
the default hierarchical Ajax-based one.

## Maintainers

Current maintainers:
* David Galeano (gxleano) - https://www.drupal.org/u/gxleano
* Alexander Dross (alexanderdross) - https://www.drupal.org/u/alexanderdross
